<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-keyboard"></i>
        Inscripción ponentes
      </div>
        <?php require "prenoticias.php"?>
        <div class="col-md-8 text-center p-2">
          <form action="phpinscripcionPonente.php" method="post" autocomplete="off">
            <div class="form-group">
              <label for="exampleInputEmail1">Apellidos</label>
              <input name="apellidos" type="text" class="form-control"  placeholder="Ingrese sus Apellidos">
            </div>
              <div class="form-group">
              <label for="exampleInputEmail1">Nombres</label>
              <input name="nombres" type="text" class="form-control" placeholder="Ingrese sus Nombres" pattern="[A-ZÁÉÍÓÚñÑ ]+" title="No se permiten números ni letras en minúsculas">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telefono</label>
              <input name="telefono" type="number" class="form-control" placeholder="Ingrese su Telefono" pattern="[0-9]+">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Documento</label>
              <input name="documento" type="number" class="form-control" placeholder="Ingrese su Documento" pattern="[0-9]+">
            </div>
            <div class="form-group">
              <input name="contrasena" type="hidden" class="form-control" value="0">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Correo</label>
              <input name="correo" type="mail" class="form-control" placeholder="Ingrese su correo">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Tipo de Documento</label>
              <select name="tipo_documento" class="form-control">
                <option value="1">CC</option>
              </select>
            </div>
            <div class="form-group">
              <input name="fkestado" type="hidden" class="form-control" value="2">
            </div>
            <div class="form-group">
              <input name="fkrol" type="hidden" class="form-control" value="2">
            </div>
            <div class="form-group">
              <input name="fkcertificado" type="hidden" class="form-control" value="2">
            </div>
            <div class="form-group">
              <input name="codverificacion" type="hidden" class="form-control" value="0">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Cuanto es 5+5</label>
              <input name="captcha" type="text" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #01b5bd;border:0px">Registrar</button>
          </form>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
