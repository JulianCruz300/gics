<?php require "preheaderDos.php";?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-home"></i>
        Inicio
      </div>
        <?php require "prenoticias.php" ?>
        <div class="col-md-8 text-center p-2">

          <?php
          include('conexion.php');
          //----
          	$sqlx10 = "SELECT * FROM usuario WHERE estado = '2'";
          if(!$resultx10 = $db->query($sqlx10)){
            die('Hay un error corriendo en la consulta o datos no encontrados!!! [' . $db->error . ']');
          }?>
          <table id="my_table" class="table table-resposive table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Documento</th>
                      <th>Correo</th>
                      <th>Telefono</th>
                      <th>Estado</th>
                  </tr>
              </thead>
              <tbody>
          <?php
          while($rowx10 = $resultx10->fetch_assoc())
          {
            			$nnombre=stripslashes($rowx10["nombre"]);
                  $aapellido=stripslashes($rowx10["apellido"]);
                  $ddocumento=stripslashes($rowx10["documento"]);
                  $ccorreo=stripslashes($rowx10["correo"]);
                  $ttelefono=stripslashes($rowx10["telefono"]);
                  $eestado=stripslashes($rowx10["estado"]);?>
                        <tr>
                            <td><?= $nnombre?></td>
                            <td><?= $aapellido?></td>
                            <td><?= $ddocumento?></td>
                            <td><?= $ccorreo?></td>
                            <td><?= $ttelefono?></td>
                            <td>
                              <form action="phppreregistrados.php" method="POST">
                                <input type="hidden" name="documento" value="<?= $ddocumento?>">
                                <select name="estado">
                                  <?php if ($eestado == 1) {?>
                                    <option value="1" selected="">Pre-registrado</option>
                                    <option value="2">Aceptado</option>
                                    <option value="3">Cancelado</option>
                                    <?php
                                  } ?>
                                   <?php if ($eestado == 2) {?>
                                    <option value="1">Pre-registrado</option>
                                    <option value="2" selected="">Aceptado</option>
                                    <option value="3">Cancelado</option>
                                    <?php
                                  } ?>
                                   <?php if ($eestado == 3) {?>
                                    <option value="1">Pre-registrado</option>
                                    <option value="2">Aceptado</option>
                                    <option value="3" selected="">Cancelado</option>
                                    <?php
                                  } ?>
                                </select>
                                <button type="submit" class="btn btn-block color-dudoso text-light mt-1">
                                  <i class="fa fa-check"></i>
                                </button>
                              </form>
                            </td>
                        </tr>
            <?php
            }
            ?>
                    </tbody>
          </table>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
<script>
  $(document).ready( function () {
    $('#my_table').DataTable();
} );
</script>
</html>
