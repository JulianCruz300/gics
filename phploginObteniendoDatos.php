<?php ob_start();?>
<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-keyboard"></i>
      </div>
        <?php require "prenoticias.php"?>
        <div class="col-md-8 text-center p-2">
            <?php
            class modificarDatos
            {
              public function getDatos($documento,$contrasena){
                require_once('conexion.php');
                // ----
                $nnombre = 'no registrado';
                $aapellido = 'no registrado';
                $ccorreo = 'no registrado';
                $ttelefono = 'no registrado';
                // -----
                $sqlx1 =  "SELECT * FROM usuario WHERE documento = '$documento'";
                if(!$resultx1 = $db->query($sqlx1)){
                  die('Hay un error corriendo en la consulta o datos no encontrados!!! [' . $db->error . ']');
                }
                while($rowx1 = $resultx1->fetch_assoc())
                {
                  // ----
                  $contrasenabd=stripslashes($rowx1["contrasena"]);
                  // ---
                  if($contrasena == $contrasenabd){
                    $_SESSION['nnombre']=stripslashes($rowx1["nombre"]);
                    $_SESSION['aapellido']=stripslashes($rowx1["apellido"]);
                    $_SESSION['ccorreo']=stripslashes($rowx1["correo"]);
                    $_SESSION['ttelefono']=stripslashes($rowx1["telefono"]);
                    // ----
                    $nnombre=$_SESSION["nnombre"];
                    $aapellido=$_SESSION["aapellido"];
                    $ccorreo=$_SESSION["ccorreo"];
                    $ttelefono=$_SESSION["ttelefono"];
                  ?>
            <form action="phpactualizarDatos.php" method="post" autocomplete="off">
              <div class="form-group">
                <input name="documento" type="hidden" class="form-control" value="<?=$_POST["documento"];?>">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Apellidos</label>
                <input name="apellidos" type="text" class="form-control" value="<?=$aapellido;?>" placeholder="Ingrese sus Apellidos" pattern="[A-ZÁÉÍÓÚñÑ ]+" title="No se permiten números ni letras en minúsculas">
              </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombres</label>
                  <input name="nombres" type="text" class="form-control" value="<?=$nnombre;?>" placeholder="Ingrese sus Nombres" pattern="[A-ZÁÉÍÓÚñÑ ]+" title="No se permiten números ni letras en minúsculas">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Correo</label>
                  <input name="correo" type="mail" class="form-control" value="<?=$ccorreo;?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de Móvil</label>
                  <input name="telefono" type="text" class="form-control" value="<?=$ttelefono;?>" placeholder="Ingrese su Documento" pattern="[0-9]+">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">¿Cuál es el resultado de 5+1?</label>
                  <input name="captcha" type="text" class="form-control" placeholder="Ingrese el resultado de la operación" pattern="[0-9]+">
                </div>
                <button type="submit" class="btn btn-primary mt-4" style="background-color: #01b5bd;border:0px">Actualizar mis Datos</button>
          </form>
          <?php
                }
                if($contrasena != $contrasenabd){?>

                    <p class="h3 mt-4">Contraseña incorrecta</p>
              <?php
                }
              }
              // --------------
            }
          }
            // ---
            $object = new modificarDatos;
            $object->getDatos($_POST["documento"],$_POST["pass"]);

          ?>
      </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
