<?php require "seguridadad.php";?>
<?php
ob_start();
?>
<?php require "preheaderDos.php";?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
<?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-home"></i>
        Certificar Participantes
      </div>
        <div class="col-md-12 mt-2 mb-2 text-center p-2">
          <?php
          include('conexion.php');
          $ccertificado="No registrado";
          // subconsulta para traer el nombre del Instructor
          	$sqlx10 = "SELECT * FROM usuario WHERE fk_estado = '2' AND fk_rol = '3'";
          if(!$resultx10 = $db->query($sqlx10)){
            die('Hay un error corriendo en la consulta o datos no encontrados!!! [' . $db->error . ']');
          }?>
         <table id="my_table" class="table table-resposive table-striped table-bordered rounded"  style="width:100%">
              <thead class="thead-dark">
                  <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Documento</th>
                      <th>Correo</th>
                      <th>Telefono</th>
                      <th>Estado</th>
                      <th>Certificación</th>
                      <th>Opciones</th>
                  </tr>
              </thead>
              <tbody class="table-hover">
          <?php
          while($rowx10 = $resultx10->fetch_assoc())
          {
            			$nnombre=stripslashes($rowx10["nombre"]);
                  $aapellido=stripslashes($rowx10["apellido"]);
                  $ddocumento=stripslashes($rowx10["documento"]);
                  $ccorreo=stripslashes($rowx10["correo"]);
                  $ttelefono=stripslashes($rowx10["telefono"]);
                  $ffkestado=stripslashes($rowx10["fk_estado"]);
                  $ffkcertificado=stripslashes($rowx10["fk_certificado"]);
				  
				  if ($ffkcertificado==1)
							{
								$visto="<img src='img/vistobueno.png' width='20'/>";
							}
							if ($ffkcertificado==2)
							{
								$visto="<img src='img/vistomalo.png' width='20'/>";
							}
				  
				  ?>
				  
                        <!-- start : subconsulta 1 -->
                        <?php
                        $sqlx11 = "SELECT * FROM estado WHERE id_estado = $ffkestado";
                        if(!$resultx11 = $db->query($sqlx11)){
                          die('Hay un error corriendo en la consulta o datos no encontrados!!! [' . $db->error . ']');
                        }
                        while($rowx11 = $resultx11->fetch_assoc()){
                            $eestado=stripslashes($rowx11["estado"]);
                        }
                        ?>
                        <?php
                        $sqlx12 = "SELECT * FROM certificado WHERE id_certificado = $ffkcertificado";
                        if(!$resultx12 = $db->query($sqlx12)){
                          die('Hay un error corriendo en la consulta o datos no encontrados!!! [' . $db->error . ']');
                        }
                        while($rowx12 = $resultx12->fetch_assoc()){
                            $ccertificado=stripslashes($rowx12["certificado"]);
							
                        }
                        ?>
                        <tr>
                            <td><?= $nnombre?></td>
                            <td><?= $aapellido?></td>
                            <td><?= $ddocumento?></td>
                            <td><?= $ccorreo?></td>
                            <td><?= $ttelefono?></td>
                            <td><?= ucwords($eestado)?></td>
                            <td><?= $visto?><?= ucwords($ccertificado)?></td>
                            <td>
                              <form action="phpcertificar.php" method="post">
                                <input type="hidden" name="documento" value="<?=$ddocumento;?>">
                                <button type="submit" class="btn color-dudoso-btn text-light mt-1 px-4 w-100"><i class="fas fa-eye-dropper" title="Preinscribir"></i> Certificar</button>
                              </form>
                              <!-- ---- -->
                              <form action="phpnocertificar.php" method="post">
                                <input type="hidden" name="documento" value="<?=$ddocumento;?>">
                                <button type="submit" class="btn color-dudoso-btn text-light mt-1 px-4 w-100"><i class="fas fa-eye-dropper" title="Preinscribir"></i> No certificar</button>
                              </form>
                            </td>
                       </tr>
            <?php
            }
            ?>
                    </tbody>
          </table>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
<script>
  $(document).ready( function () {
    $('#my_table').DataTable();
} );
</script>
</html>
