<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-suitcase"></i>
        Participante
      </div>
        <?php require "prenoticias.php" ?>
        <div class="col-md-8 text-center p-2">
          <form action="phploginObteniendoDatos.php" method="POST" autocomplete="off">
            <div class="form-group">
              <label for="exampleInputEmail1">Documento del solicitante</label>
              <p class="h4"><?= $_POST['documento'];?></p>
            <input type="hidden" name="documento" class="form-control"  value="<?=$_POST['documento'];?>">
            </div>
            <div class="form-group">
			</br>
              <label for="exampleInputEmail1">Por favor digite su Contraseña</label>
              <input type="text" autofocus name="pass" class="form-control" aria-describedby="emailHelp" placeholder="Ingrese su Contraseña">
              <a href="prerecordarcontrasena.php">¿Olvidó su contraseña?</a></div>
            <div class="form-group">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #01b5bd;border:0px">Modificar mis datos</button>
          </form>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
