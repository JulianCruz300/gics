<footer class="text-center py-3">
<div class="container ">
	<div class="row">
			<div class="col-md-12">
				<small>
				Servicio Nacional de Aprendizaje SENA <a href="preformadministracion.php" style="color: white">Administración</a> - Dirección General
				Calle 57 No. 8 - 69 Bogotá D.C. (Cundinamarca), Colombia
				Conmutador Nacional (57 1) 5461500 - Extensiones
				Atención presencial: lunes a viernes 8:00 a.m. a 5:30 p.m. - Resto del país sedes y horarios
				Atención telefónica: lunes a viernes 7:00 a.m. a 7:00 p.m. - sábados 8:00 a.m. a 1:00 p.m.
				Atención al ciudadano: Bogotá (57 1) 5925555 - Línea gratuita y resto del país 018000 910270
				Atención al <a class="text-light" href="preformsuperadmin.php">empresario</a>: Bogotá (57 1) 4049494 - Línea gratuita y resto del país 018000 910682
				</small>
			</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="datatable/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="datatable/spanish.json"></script>



</footer>
