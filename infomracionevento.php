<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="favicon4.ico">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<table border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <col width="111" />
  <col width="302" />
  <col width="283" />
  <col width="313" />
  <tr height="26">
    <td colspan="4" height="26" width="1009">Planeaci&oacute;n Agenda Segunda Feria Tecnol&oacute;gica Proyecta 2019 - 22    de Marzo 2019</td>
  </tr>
  <tr height="21">
    <td height="21" width="111">Hora</td>
    <td width="302">Agenda</td>
    <td width="283">Lugar</td>
    <td width="313">Responsable</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">7:30 -    8:30</td>
    <td>Registro de asistentes</td>
    <td width="283">Ingreso    auditorio B</td>
    <td width="313">Susana    Escarraga</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">8:30 - 8:45</td>
    <td>Saludo de apertura</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Subdirectora    Dra. Claudia Yannet G&oacute;mez Larrota</td>
  </tr>
  <tr height="40">
    <td height="40" width="111">8:45 - 9:00</td>
    <td>Presentaci&oacute;n del &aacute;rea    I+D+i GICS</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Mauricio    Vargas<br />
      Robinson Castillo</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">9:00 - 9:20</td>
    <td>Big Data</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructor    Jairo Vel&aacute;squez Bustos</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">9:20 - 9:40</td>
    <td>Blockchain</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructora    Sonia C&aacute;rdenas</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">9:40 - 10:00</td>
    <td>Econom&iacute;a Naranja&nbsp;</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructora    Andrea Camacho</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">10:00 - 10:30</td>
    <td colspan="3">Receso</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">10:30 - 11:00</td>
    <td>Taller Eficiencia    Energ&eacute;tica</td>
    <td width="283">Ambiente    156</td>
    <td width="313">Instructor    Yovany Caro</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">9:00 - 11:00</td>
    <td>Feria de proyectos    TIC: Socializaci&oacute;n fichas</td>
    <td width="283">Auditorio    C</td>
    <td width="313">Semilleros    de investigaci&oacute;n</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">11:00 - 12:30</td>
    <td>Feria de proyectos    TIC: Evaluaci&oacute;n y Premiaci&oacute;n</td>
    <td width="283">Auditorio    C</td>
    <td width="313">Semilleros    de investigaci&oacute;n</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">12:30 - 13:30</td>
    <td colspan="3">Almuerzo</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">13:30 - 13:50</td>
    <td>Industria 4.0 /    Internet Industrial:</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Camila    Espitia</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">13:50 - 14:10</td>
    <td>Realidad virtual /    aumentada</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructora    Leydy Nahir Fonseca C&aacute;rdenas</td>
  </tr>
  <tr height="40">
    <td height="40" width="111">14:10 - 14:30</td>
    <td>Internet de la cosas    / Computaci&oacute;n en la nube</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructora    Jasvleidy Fajardo<br />
      Andre Laverde</td>
  </tr>
  <tr height="40">
    <td height="40" width="111">14:30 - 14:50</td>
    <td>RITEL</td>
    <td width="283">Auditorio    B</td>
    <td width="313">Instructor    Kelvin Casta&ntilde;eda<br />
      Hernando Piracoca</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">15:00 - 15:30</td>
    <td>Taller Compatibilidad    electromagn&eacute;tica</td>
    <td width="283">Ambiente    156</td>
    <td width="313">Instructor    Arley Delgado</td>
  </tr>
  <tr height="20">
    <td height="20" width="111">15:30</td>
    <td colspan="3">Cierre del evento</td>
  </tr>
</table>
</body>
</html>
