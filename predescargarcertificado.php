<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-suitcase"></i>
        Descargar Certificado
      </div>
        <?php require "prenoticias.php" ?>
        <div class="col-md-8 text-center p-2">
          <form action="phpdescargarcertificado.php" autocomplete="OFF" method="POST" autofocus="OFF" target="_blank">
            <div class="form-group">
              <label for="exampleInputEmail1">N&uacute;mero de Documento </label>
              <input type="text" name="documento" autofocus class="form-control" aria-describedby="emailHelp" placeholder="Ingrese su n&uacute;mero de Documento" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">&iquest;Cu&aacute;l es el resultado de 2+5?</label>
              <input name="captcha" type="text" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #01b5bd;border:0px">Generar mi Certificado</button>
          </form>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
