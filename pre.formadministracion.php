<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-suitcase"></i>
        Administración
      </div>
        <?php require "prenoticias.php" ?>
        <div class="col-md-8 text-center p-2">
          <form action="phpformadministracion.php" method="POST">
            <div class="form-group">
              <label for="exampleInputEmail1">Documento</label>
              <input type="number" name="documento" class="form-control" aria-describedby="emailHelp" placeholder="Ingrese su Documento">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" class="form-control" name="pass" placeholder="Ingrese su Contraseña">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #01b5bd;border:0px">Ingresar</button>
          </form>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
