<?php require "preheader.php" ?>
  <div class="container width-container">
  <div class="row">
    <div class="col-md-12 text-center">
      <?php require "slider.php" ?>
    </div>
  </div>
    <div class="row pt-4">
      <div class="col-md-12 titlesena">
        <i class="fas fa-keyboard"></i>
        Preinscripción
      </div>
        <?php require "prenoticias.php"?>
        <div class="col-md-8 text-center p-2">
          <form action="phppreinscripcion.php" method="post" autocomplete="off">
            <div class="form-group">
              <label for="exampleInputEmail1">Apellidos (Sólo Mayúsculas)</label>
              <input autofocus name="apellidos" type="text" class="form-control"  placeholder="Ingrese sus Apellidos" required>
            </div>
              <div class="form-group">
              <label for="exampleInputEmail1">Nombres (Sólo Mayúsculas)</label>
              <input name="nombres" type="text" class="form-control" placeholder="Ingrese sus Nombres" pattern="[A-ZÁÉÍÓÚñÑ ]+" title="No se permiten números ni letras en minúsculas" required>
            </div>
			
			<div class="form-group">
              <label for="exampleInputEmail1">Tipo de Documento</label>
              <select name="tipo_documento" class="form-control" required>
                <option value="">Seleccione:</option>
				<option value="1">CC</option>
				<option value="2">CE</option>
				<option value="3">TI</option>
              
			  </select>
            </div>
			
			<div class="form-group">
              <label for="exampleInputEmail1">Documento (Sólo Números)</label>
              <input name="documento" type="text" class="form-control" placeholder="Ingrese su Documento" pattern="[0-9]+" required>
            </div>
			
			
            <div class="form-group">
              <label for="exampleInputEmail1">Número de Móvil</label>
              <input name="telefono" type="text" class="form-control" placeholder="Ingrese su número celular" pattern="[0-9]+" required>
            </div>
            
            <div class="form-group">
              <label for="exampleInputEmail1">Contraseña (Sólo letras y números)</label>
              <input name="contrasena" type="password" class="form-control" placeholder="Ingrese su Contraseña" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Confirmar contraseña</label>
              <input name="repass" type="password" class="form-control" placeholder="Confirme su Contraseña" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Correo</label>
              <input name="correo" type="mail" class="form-control" placeholder="Ingrese su correo" required>
            </div>
            
			
			
			
			
			
			
			
            <div class="form-group">
              <input name="fkestado" type="hidden" class="form-control" value="1">
            </div>
            <div class="form-group">
              <input name="fkrol" type="hidden" class="form-control" value="3">
            </div>
            <div class="form-group">
              <input name="fkcertificado" type="hidden" class="form-control" value="2">
            </div>
            <div class="form-group">
              <input name="codverificacion" type="hidden" class="form-control" value="0">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">¿Cuál es el resultado de 2+2?</label>
              <input name="captcha" type="text" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #01b5bd;border:0px">Preinscribirme</button>
          </form>
        </div>
    </div>
  </div>
</body>
<?php require "prefooter.php" ?>
<script src="js/bootstrap.min.js"></script>
</html>
