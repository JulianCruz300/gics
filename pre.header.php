<!DOCTYPE html>
<html lang="en">

<head>
<link rel="shortcut icon" href="favicon4.ico">
    <meta charset="UTF-8">
    <title>ProyectaIDT</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- -- -->
</head>
<body>
    <!-- start: navbar -->
                <!---->
                  <ul class=" nav justify-content-center py-2 nav-tabs color-dudoso text-color sticky-top py-3">
                      <!-- end: Home-->
                          <li class="nav-item">
                            <a class="nav-link stl-nv-a" href="index.php">
                              Home <i class="fas fa-home"></i></a>
                          </li>
                          <!-- start: Home-->
                          <!-- start: Preinscripción-->
                              <li class="nav-item">
                                  <a class="nav-link stl-nv-a" href="prepreinscripcion.php">
                                  Preinscripción <i class="fas fa-keyboard"></i></a>
                              </li>
                          <!-- end: Preinscripción-->
                          <!-- start: Consultar preinscripcioón-->
                              <li class="nav-item">
                                  <a class="nav-link stl-nv-a" href="preconsultarPreinscripcion.php">
                                  Consultar Preinscripción <i class="fas fa-search"></i></a>
                              </li>
                          <!-- end: Consultar preinscripcioón-->
                          <!-- start: Informació del evento-->
                              <li class="nav-item">
                                  <a class="nav-link stl-nv-a" href="#">
                                  Info.Evento <i class="fas fa-info-circle"></i></a>
                              </li>
                          <!-- end: Informació del evento-->
                          <!-- start: generar certificado -->
                          <li class="nav-item">
                            <a class="nav-link stl-nv-a" href="predescargarcertificado.php">
                              Descargar certificado <i class="fas fa-file-download"></i></a>
                            </l>
                            <!-- end : generar certificado -->
                          <!-- start: Proyectos -->
                                  <li class="nav-item">
                                      <a class="nav-link stl-nv-a" href="#">
                                      Contactenos <i class="fas fa-phone"></i></a>
                                  </l>
                          <!-- end : Contacto -->
                  </ul>
                <!---->

    <!-- end: navbar -->
