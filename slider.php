<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="favicon4.ico">
	<title>SENA Ceet</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Slider" />
	
	<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head>
<link rel="shortcut icon" href="favicon4.ico"> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->

</head>
<body>
	
	<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
		<li><img src="data1/images/ceet.jpg" alt="CEET" title="CEET" id="wows1_0"/></li>
		<li><img src="data1/images/desarrollo.jpg" alt="INVESTIGACI&Oacute;N" title="INVESTIGACI&Oacute;N" id="wows1_1"/></li>
		<li><img src="data1/images/gics.jpg" alt="SENNOVA" title="SENNOVA" id="wows1_2"/></li>
		<li><img src="data1/images/sena.jpg" alt="SENA" title="SENA" id="wows1_3"/></li>
	</ul></div>
	<div class="ws_bullets"><div>
		<a href="#" title="CEET"><span><img src="data1/tooltips/ceet.jpg" alt="CEET"/>1</span></a>
		<a href="#" title="INVESTIGACI&Oacute;N"><span><img src="data1/tooltips/desarrollo.jpg" alt="INVESTIGACI&Oacute;N"/>2</span></a>
		<a href="#" title="SENNOVA"><span><img src="data1/tooltips/gics.jpg" alt="SENNOVA"/>3</span></a>
		<a href="#" title="SENA"><span><img src="data1/tooltips/sena.jpg" alt="SENA"/>4</span></a>
	</div></div>
	<!--<div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">bootstrap carousel example</a> by WOWSlider.com v8.8</div>-->
	<div class="ws_shadow"></div>
	</div>	
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
	<!-- End WOWSlider.com BODY section -->

</body>
</html>